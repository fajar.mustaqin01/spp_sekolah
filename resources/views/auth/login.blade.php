@extends('layouts.appLogin')

@section('content')
<div class="account-content">
    <div class="login-wrapper">
        <div class="login-content">
            <div class="login-userset">
                <div class="login-logo">
                    <img src="{{ asset('assets/img/logo_tk.jpeg')}}" style="width: 100px; height: 100px;" alt="img">
                </div>
                <div class="login-userheading">
                    <h3>Log In</h3>
                </div>
                <form action="{{ route('login')}}" method="POST" data-parsley-validate>
                    @csrf
                    @if (session()->has('error'))
                        <p class="text-danger">{{session('error')}} </p>
                    @endif
                    <div class="form-login">
                        <label>Email</label>
                        <div class="form-addons">
                            <input type="email" name="email" data-parsley-trigger="change" placeholder="Masukaan email anda" required>
                            <img src="{{ asset('assets/img/icons/mail.svg')}}" alt="img">
                        </div>
                    </div>
                    <div class="form-login">
                        <label>Password</label>
                        <div class="pass-group">
                            <input type="password" name="password" class="pass-input" placeholder="Masukan password anda" required>
                            <span class="fas toggle-password fa-eye-slash"></span>
                        </div>
                    </div>
                    <div class="form-login">
                        <button class="btn btn-login" type="submit">{{ __('Login') }}</button>                         
                    </div>
                </form>
            </div>
        </div>
        <div class="login-img">
            <img src="{{ asset('assets/img/login.jpg')}}" alt="img">
        </div>
    </div>
</div>

@endsection