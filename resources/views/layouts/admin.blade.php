<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    <title>@yield('title') | Pembayaran SPP</title>

    @stack('prepend-style')
    @include('includes.style')
    @stack('addon-style')
</head>
<body>
    <div id="global-loader">
        <div class="whirly-loader"> </div>
    </div>

    <div class="main-wrapper">

    @include('includes.header')

    @include('includes.sidebar')

    @yield('content')
    
    @stack('prepend-script')
    @include('includes.script') 
    @stack('addon-script')
    @include('sweetalert::alert')
</body>
</html>