@extends('layouts.admin')
@section('title', 'Data Kelas')

@section('content')

<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Data Kelas</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Kelas</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a href="{{ url('kelas/create')}}" class="btn btn-added" data-bs-toggle="modal" data-bs-target="#addkelas"><img src="assets/img/icons/plus.svg" alt="img">Tambah Kelas</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Kelas TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table  datanew ">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kelas</th>
                                        <th>Tingkatan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kelas as $i => $kel)
                                        <tr>
                                            <td>{{ $i += 1}}</td>
                                            <td>{{ $kel->nama_kelas}}</td>
                                            <td>{{ $kel->tingkat}}</td>

                                            <td>
                                                <a class="me-3" href="{{ route('kelas.edit', $kel->id_kelas)}}">
                                                    <img src="assets/img/icons/edit.svg" alt="img">
                                                </a>
                                                <a href="javascript:void(0);">
                                                    <img src="assets/img/icons/delete.svg" alt="img">
                                                </a>
                                                {{-- <form action="{{url('data-siswa', $sis->id_siswa)}}" method="POST" class="me-3 confirm-text" >
                                                    @csrf
                                                    @method('delete')
                                                </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

<div class="modal fade" id="addkelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('kelas.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nama Kelas</label>
                                <input type="text" name="nama_kelas" id="nama_kelas">
                            </div>
                        </div>
        
                        <div class="col-12">
                            <div class="form-group">
                                <label>Tingkat</label>
                                <input type="text" name="tingkat" id="tingkat">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-submit">Confirm</button>
                        <button type="button" class="btn btn-cancel" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
        </div>
    </div>
</div>
    
@endsection