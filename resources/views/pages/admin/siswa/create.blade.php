@extends('layouts.admin')
@section('title', 'Tambah Siswa')

@section('content')

<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Data Siswa</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('data-siswa')}}">Data Siswa</a></li>
                        <li class="breadcrumb-item active">Tambah Siswa</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Siswa TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <form action="{{route('data-siswa.store')}}" method="post">
                                @csrf
                                <div class="col-lg-4 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>NIS</label>
                                        <input type="text" name="id_siswa">
                                    </div>
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="nama">
                                    </div>
                                </div>
    
                                <div class="col-lg-4 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Tanggal Lahir </label>
                                        <div class="input-groupicon">
                                            <input type="text" placeholder="DD-MM-YYYY" name="tgl_lahir" class="datetimepicker">
                                            <div class="addonset">
                                            <img src="{{ asset('assets/img/icons/calendars.svg')}}" alt="img">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kelas</label>
                                        <select class="form-select" name="id_kelas">
                                            <option selected disabled>--Pilih Kelas--</option>
                                            @foreach ($kelas as $key => $val)
                                                <option value="{{$val['id']}}">{{$val['nama_kelas']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
    
                                <div class="col-lg-4 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>No HP</label>
                                        <input type="number" name="no_hp">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea name="alamat" id="alamat" name="alamat" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
    
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-submit me-2">Simpan</a>
                                    <a href="{{route('data-siswa')}}" class="btn btn-cancel">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    
    </div>
</div>
    
@endsection