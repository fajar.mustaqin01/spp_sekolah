@extends('layouts.admin')
@section('title', 'Data Siswa')

@section('content')

<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Data Siswa</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Siswa</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a href="{{ route('data-siswa.create') }}" class="btn btn-added"><img src="assets/img/icons/plus.svg" alt="img">Tambah Siswa</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Siswa TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table  datanew ">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Alamat</th>
                                        <th>No HP</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($siswa as $i => $sis)
                                        <tr>
                                            <td>{{ $i += 1}}</td>
                                            <td>{{ $sis->id_siswa}}</td>
                                            <td>{{ $sis->nama}}</td>
                                            <td>{{ $sis->kelas->nama_kelas}}</td>
                                            <td>{{ $sis->tgl_lahir}}</td>
                                            <td>{{ $sis->alamat}}</td>
                                            <td>{{ $sis->no_hp}}</td>

                                            <td>
                                                <a class="me-3" href="{{ route('data-siswa.edit', $sis->id_siswa)}}">
                                                    <img src="assets/img/icons/edit.svg" alt="img">
                                                </a>
                                                <a href="javascript:void(0);">
                                                    <img src="assets/img/icons/delete.svg" alt="img">
                                                </a>
                                                {{-- <form action="{{url('data-siswa', $sis->id_siswa)}}" method="POST" class="me-3 confirm-text" >
                                                    @csrf
                                                    @method('delete')
                                                </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
    
@endsection