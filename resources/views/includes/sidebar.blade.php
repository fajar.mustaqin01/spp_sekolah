<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="active">
                    <a href="{{ url('home')}}"><i data-feather="home"></i><span> Beranda</span> </a>
                </li>
                
                <li class="submenu">
                    <a href="javascript:void(0);"><i data-feather="user"></i><span> Siswa</span> <span class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="quotationList.html">Quotation List</a></li>
                        <li><a href="addquotation.html">Add Quotation</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url('data-siswa') }}"><i data-feather="user"></i><span> Data Siswa</span> </a>
                </li>

                <li>
                    <a href="{{ url('kelas')}}"><i data-feather="users"></i><span> Kelas</span> </a>
                </li>

                <li>
                    <a href="{{ url('spp')}}"><i data-feather="package"></i><span> SPP</span> </a>
                </li>

                <li>
                    <a href="{{ url('pembayaran')}}"><i data-feather="tag"></i><span> Pembayaran</span> </a>
                </li>

                <li>
                    <a href="{{ url('users')}}"><i data-feather="settings"></i><span> Pengguna Sistem</span> </a>
                </li>
    
            </ul>
        </div>
    </div>
</div>