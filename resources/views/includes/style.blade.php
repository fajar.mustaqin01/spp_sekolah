<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.jpg')}}">

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.min.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css')}}">

<link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}">
<link rel="stylesheet" href=" {{ asset('assets/plugins/fontawesome/css/all.min.css')}}">

<link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">

<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css')}}">


