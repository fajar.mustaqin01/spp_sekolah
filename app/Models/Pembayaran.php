<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    use HasFactory;

    protected $table = 'pembayaran';

    protected $primaryKey = 'id_pembayaran';

    protected $fillable = [
        'tanggal_pembayaran',
        'jumlah_bayar',
        'metode_pembayaran',
        'id_spp',
        'id_petugas'
    ];
}
