<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $table = 'siswa';

    protected $primaryKey = 'id_siswa';

    protected $fillable = [
        'nama',
        'tgl_lahir',
        'alamat',
        'no_hp',
        'id_kelas'
    ];

    public function kelas(){
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }
}
