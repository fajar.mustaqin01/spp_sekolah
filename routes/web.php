<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\SiswaController;
use App\Http\Controllers\Admin\KelasController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/login', [App\Http\Controllers\LoginController::class, 'index'])->name('login');

Route::post('/login', [App\Http\Controllers\LoginController::class, 'postLogin'])->name('postLogin');

Route::resource('/data-siswa', SiswaController::class);
Route::get('/data-siswa/create', [SiswaController::class, 'create'])->name('data-siswa.create');
// Route::get('/data-siswa', [App\Http\Controllers\Admin\SiswaController::class, 'index'])->name('data-siswa');


Route::resource('/kelas', KelasController::class);